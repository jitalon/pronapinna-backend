package com.hellokoding.auth.objects;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class Notificacion {

	private int idPlanNacionalAnual;

	public int getIdPlanNacionalAnual() {
		return idPlanNacionalAnual;
	}

	public void setIdPlanNacionalAnual(int idPlanNacionalAnual) {
		this.idPlanNacionalAnual = idPlanNacionalAnual;
	}

	private int idActividad;

	public int getIdActividad() {
		return idActividad;
	}

	public void setIdActividad(int idActividad) {
		this.idActividad = idActividad;
	}

	private String tipoRespuesta;

	public String getTipoRespuesta() {
		return tipoRespuesta;
	}

	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}

	private String dependenciaCoodinador;

	public String getDependenciaCoodinador() {
		return dependenciaCoodinador;
	}

	public void setDependenciaCoodinador(String dependenciaCoodinador) {
		this.dependenciaCoodinador = dependenciaCoodinador;
	}

	private String dependencia;

	public String getDependencia() {
		return dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	private String dependenciadescripcion;

	public String getDependenciaDescripcion() {
		return dependenciadescripcion;
	}

	public void setDependenciaDescripcion(String dependenciadescripcion) {
		this.dependenciadescripcion = dependenciadescripcion;
	}

	private String accionPuntual;

	public String getAccionPuntual() {
		return accionPuntual;
	}

	public void setAccionPuntual(String accionPuntual) {
		this.accionPuntual = accionPuntual;
	}

	private String estrategia;

	public String getEstrategia() {
		return estrategia;
	}

	public void setEstrategia(String estrategia) {
		this.estrategia = estrategia;
	}

	private String objetivo;

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	private String actividad;

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	private String comentario;

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	private String estatus;

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	private String claveAccion;

	public String getClaveAccion() {
		return claveAccion;
	}

	public void setClaveAccion(String claveAccion) {
		this.claveAccion = claveAccion;
	}

	private String claveEst;

	public String getClaveEst() {
		return claveEst;
	}

	public void setClaveEst(String claveEst) {
		this.claveEst = claveEst;
	}

	private String claveObj;

	public String getClaveObj() {
		return claveObj;
	}

	public void setClaveObj(String claveObj) {
		this.claveObj = claveObj;
	}

	private String usuario;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	private String fechaTabla;

	public String getFechaTabla() {
		return fechaTabla;
	}

	public void setFechaTabla(String fechaTabla) {
		this.fechaTabla = fechaTabla;
	}

}
