package com.hellokoding.auth.objects;

import java.util.List;

public class TmpLoadFile {

    private String claveEst;
    private String descEst;
    private String claveObj;
    private String descObj;
    private String claveAccion;
    private String descAccion;
    private String dependencia;
    private String rol;

	public String getClaveEst() {
		return claveEst;
	}
	public void setClaveEst(String claveEst) {
		this.claveEst = claveEst;
	}

	public String getDescEst() {
		return descEst;
	}
	public void setDescEst(String descEst) {
		this.descEst = descEst;
	}

	public String getClaveObj() {
		return claveObj;
	}
	public void setClaveObj(String claveObj) {
		this.claveObj = claveObj;
	}

	public String getDescObj() {
		return descObj;
	}
	public void setDescObj(String descObj) {
		this.descObj = descObj;
	}

	public String getClaveAccion() {
		return claveAccion;
	}
	public void setClaveAccion(String claveAccion) {
		this.claveAccion = claveAccion;
	}

	public String getDescAccion() {
		return descAccion;
	}
	public void setDescAccion(String descAccion) {
		this.descAccion = descAccion;
	}

	public String getDependencia() {
		return dependencia;
	}
	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}  

	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}  
}