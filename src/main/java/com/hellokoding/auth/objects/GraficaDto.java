package com.hellokoding.auth.objects;

import java.util.Date;

public class GraficaDto {

	private Date fecha;
	private String grafica1;
	private String grafica2;
	private String reporte2;

	public String getReporte2() {
		return reporte2;
	}
	public void setReporte2(String reporte2) {
		this.reporte2 = reporte2;
	}

	public String getGrafica2() {
		return grafica2;
	}
	public void setGrafica2(String grafica2) {
		this.grafica2 = grafica2;
	}

	public String getGrafica1() {
		return grafica1;
	}
	public void setGrafica1(String grafica1) {
		this.grafica1 = grafica1;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}