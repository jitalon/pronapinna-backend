package com.hellokoding.auth.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;

@Component
public class LoginSuccessHandler  extends SimpleUrlAuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		SessionFlashMapManager sessionFlashMapManager = new SessionFlashMapManager();
		FlashMap flashMap = new FlashMap();
		if(authentication!=null) {
			logger.info("Hola "+authentication.getName()+" haz iniciado sesión con exito");
			flashMap.put("success", "Hola "+authentication.getName()+" haz iniciado sesión con exito");
		}else {
			flashMap.put("success", "Hola "+" "+" haz iniciado sesión con exito");
		}
		
		sessionFlashMapManager.saveOutputFlashMap(flashMap, request, response);
		
		
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
}
