package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.hellokoding.auth.modelo.entities.CatClasificacionEdad;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface CatClasificacionEdadRepository extends PagingAndSortingRepository<CatClasificacionEdad, Integer> {

}