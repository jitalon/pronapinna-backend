package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.hellokoding.auth.modelo.entities.TblPeriodo;
import java.util.List;

public interface TblPeriodoRepository extends CrudRepository<TblPeriodo, Integer> {

    @Query("SELECT d FROM TblPeriodo d WHERE d.idPlanNacional = ?1 ORDER BY d.periodoFin DESC")
    List<TblPeriodo> findAllActivo(int idPlanNacional);

    @Query(value = "SELECT d.* FROM pronapinna.tbl_periodo d WHERE id_estatus = 1 and NOW() between d.periodo_ini and d.periodo_fin ", nativeQuery = true)
    List<TblPeriodo> getTblPeriodoActivo();

}