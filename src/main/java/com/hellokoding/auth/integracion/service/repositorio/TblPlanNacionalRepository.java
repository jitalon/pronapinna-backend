package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.jpa.repository.query.Procedure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.hellokoding.auth.modelo.entities.TblPlanNacional;

public interface TblPlanNacionalRepository extends JpaRepository<TblPlanNacional, Integer> {

    @Query("SELECT count(d) FROM TblPlanNacional d WHERE d.idEstatus = 1")
    int planNacionalActivo();

    @Transactional
    @Modifying
    @Query(value = "UPDATE TblPlanNacional t set t.idEstatus =2 where t.idEstatus = 1")
    void actualizaEstatusTblPlanNacional();


}