package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblArchivo;

interface TblArchivoRepositoryCustom {

	int insertArchivo(TblArchivo entity);
	
}