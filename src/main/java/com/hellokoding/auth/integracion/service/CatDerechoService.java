package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatDerechoRepository;

import com.hellokoding.auth.modelo.entities.CatDerecho;

import java.util.List;
import java.util.Optional;

@Component
public class CatDerechoService {

@Autowired 
CatDerechoRepository repository;

    public List<CatDerecho> getCatDerecho() {
        return (List<CatDerecho>) repository.findAll();
    }

    public List<CatDerecho> getCatDerechoActivo() {
        return (List<CatDerecho>) repository.findAllActivo();
    }

    public void add(CatDerecho dto) {
        repository.save(dto);
    }
  
}