package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import com.hellokoding.auth.modelo.entities.OprActividad;

public interface OprActividadRepository extends JpaRepository<OprActividad, Integer> {

@Query("SELECT d FROM OprActividad d WHERE d.tblPlanNacionalAnual.id = ?1 and d.catActividad.id = ?2")
    List<OprActividad> findActividad( int IdPlanNacional, int IdActividad);

@Transactional
@Modifying
@Query(value = "UPDATE OprActividad u set u.idEstatus =?1 where u.catActividad.id = ?2 and u.tblPlanNacionalAnual.id = ?3")
//,nativeQuery = true)
    void actualizaEstatusOprActividad(int estatus, int idActividad, int idPlanNacional);

}


    