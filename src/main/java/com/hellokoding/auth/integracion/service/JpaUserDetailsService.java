package com.hellokoding.auth.integracion.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hellokoding.auth.integracion.service.repositorio.TblUsuarioRepository;
import com.hellokoding.auth.modelo.entities.CatPerfil;
import com.hellokoding.auth.modelo.entities.TblUsuario;

@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService{

	@Autowired
	private TblUsuarioRepository tblUsuarioRepository;
	
	private Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		TblUsuario usuario = tblUsuarioRepository.findByUsuario(username);
		
		if(usuario==null) {
			logger.error("Error login: no existe el usuario: "+username);
			throw new UsernameNotFoundException("El usuario "+username+" no existe");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		
		CatPerfil role = usuario.getCatPerfil();
		logger.info("Perfil: "+role.getClave());
		authorities.add(new SimpleGrantedAuthority(role.getClave()));
		if(authorities.isEmpty()) {
			logger.error("Error login: el usuario: "+username+" no tiene perfil asignado");
			throw new UsernameNotFoundException("Error login: el usuario: "+username+" no tiene perfil asignado");
		}
		return new User(usuario.getUsuario(), usuario.getPassword(), (usuario.getIdEstatus()==1), true, true, true, authorities);
	}

}
