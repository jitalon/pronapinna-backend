package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatClasificacionEdadRepository;

import com.hellokoding.auth.modelo.entities.CatClasificacionEdad;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@Component
public class CatClasificacionEdadService {

@Autowired 
CatClasificacionEdadRepository repository;

    public List<CatClasificacionEdad> getCatClasificacionEdad() {
        return (List<CatClasificacionEdad>) repository.findAll(Sort.by("id").ascending());
    }

    public void add(CatClasificacionEdad dto) {
        repository.save(dto);
    }
  
}