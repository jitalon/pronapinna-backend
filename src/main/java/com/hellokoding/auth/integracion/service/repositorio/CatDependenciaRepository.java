package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.repository.CrudRepository;
import com.hellokoding.auth.modelo.entities.CatDependencia;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

@Repository
public interface CatDependenciaRepository extends CrudRepository<CatDependencia, Integer>{

    @Query("SELECT d FROM CatDependencia d WHERE d.clave = ?1")
    List<CatDependencia> findByClave(String clave);

}

