package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TmpTblEstrategiaRepository;

import com.hellokoding.auth.modelo.entities.TmpTblEstrategia;

import java.util.List;
import java.util.Optional;

@Component
public class TmpTblEstrategiaService {

@Autowired 
TmpTblEstrategiaRepository repository;

    public void add(TmpTblEstrategia dto) {
        repository.save(dto);
    }  
}