package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatPerfil;

public interface CatPerfilRepository extends RepositoryGeneric<CatPerfil, Integer> {

}