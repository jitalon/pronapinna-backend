package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblLineaAccionRepository;

import com.hellokoding.auth.modelo.entities.TblLineaAccion;

import java.util.List;
import java.util.Optional;

@Component
public class TblLineaAccionService {

@Autowired 
TblLineaAccionRepository repository;

    public List<TblLineaAccion> getTblLineaAccion() {
        return (List<TblLineaAccion>) repository.findAll();
    }

    public List<TblLineaAccion> getTblLineaAccionActivo() {
        return (List<TblLineaAccion>) repository.findAllActivo();
    }

    public void add(TblLineaAccion dto) {
        repository.save(dto);
    }  
}