package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TmpTblLineaAccion;

public interface TmpTblLineaAccionRepository extends RepositoryGeneric<TmpTblLineaAccion, Integer> {

}