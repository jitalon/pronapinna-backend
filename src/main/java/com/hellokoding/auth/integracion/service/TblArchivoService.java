package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblArchivoRepository;

import com.hellokoding.auth.modelo.entities.TblArchivo;

import java.util.List;
import java.util.Optional;

@Component
public class TblArchivoService {

@Autowired 
TblArchivoRepository repository;

    public List<TblArchivo> getTblArchivo() {
        return (List<TblArchivo>) repository.findAll();
    }
    
    public Optional<TblArchivo> getTblArchivoByI(int id) {
        return repository.findById(id);
    }

    public void add(TblArchivo dto) {
        repository.saveAndFlush(dto);
    }

	public void insertArchivo(TblArchivo tblArchivoNew) {
		repository.insertArchivo(tblArchivoNew);
	}
  
}