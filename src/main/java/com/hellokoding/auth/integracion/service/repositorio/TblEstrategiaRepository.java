package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblEstrategia;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface TblEstrategiaRepository extends RepositoryGeneric<TblEstrategia, Integer> {

    @Query("SELECT d FROM TblEstrategia d WHERE d.idEstatus = 1")
    List<TblEstrategia> findAllActivo();

}