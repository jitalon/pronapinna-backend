package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.jpa.repository.query.Procedure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Date;
import java.util.Collection;

import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
import com.hellokoding.auth.modelo.entities.OprActividad;

public interface TblPlanNacionalAnualRepository extends JpaRepository<TblPlanNacionalAnual, Integer>, TblPlanNacionalAnualRepositoryCustom {

    @Query("SELECT d FROM TblPlanNacionalAnual d INNER JOIN d.catDependenciaCoodinadora cd WHERE d.idEstatus = 1 AND cd.id = ?1")
    List<TblPlanNacionalAnual> findByIdDependencia(int idDependencia);

    @Query("SELECT d FROM TblPlanNacionalAnual d WHERE d.idEstatus = 1")
    List<TblPlanNacionalAnual> findAllActivo();

    @Query("SELECT count(d) FROM TblPlanNacionalAnual d WHERE d.fechaFin BETWEEN ?1 AND ?2 ")
    int planNacionalAnualCreado(Date fechaInicio, Date fechaFin);

    @Query("SELECT cd FROM TblPlanNacionalAnual d INNER JOIN d.oprActividad cd WHERE cd.idEstatus IN ?1")
    List<OprActividad> findByIdActividadEstatus(Collection<Integer> estatus);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE TblPlanNacionalAnual t set t.fechaInicio = ?1 , t.fechaFin = ?2 where t.idEstatus = 1")
    void actualizaFechas(Date fechaIni, Date fechaFin);

}