package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatDependenciaRepository;

import com.hellokoding.auth.modelo.entities.CatDependencia;
//import com.hellokoding.auth.modelo.entities.TblArchivo;

import java.util.List;
import java.util.Optional;

@Component
public class CatDependenciaService {

	@Autowired
	CatDependenciaRepository repository;

	public void add(CatDependencia dto) {
		System.out.println("+++++++++++++++++++++++++++++++++++++ AQUI");
		repository.save(toEntity(dto));
	}

	public void delete(int id) {
		repository.deleteById(id);
	}

	public List<CatDependencia> getCatDependencia() {
		return (List<CatDependencia>) repository.findAll();
	}

	public CatDependencia getCatDependenciaById(int id) {
		Optional<CatDependencia> value = repository.findById(id);
		CatDependencia optCatDependencia = new CatDependencia();
		if (value.isPresent()) {
			optCatDependencia = value.get();
		}
//        CatDependencia optCatDependencia = repository.findById(id).get();
		return optCatDependencia;
	}

	public List<CatDependencia> getCatDependenciaByClave(String clave) {
		return (List<CatDependencia>) repository.findByClave(clave);
	}

	private CatDependencia toEntity(CatDependencia dto) {
		CatDependencia entity = new CatDependencia();
		entity.setId(dto.getId());
		entity.setClave(dto.getClave());
		entity.setDescripcion(dto.getDescripcion());
		return entity;
	}

}