package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblUsuarioRepository;
import com.hellokoding.auth.modelo.entities.TblUsuario;
import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

import java.util.List;
import java.util.Optional;

@Component
public class TblUsuarioService {

	@Autowired
	TblUsuarioRepository repository;

	public void add(TblUsuario dto) {
		repository.save(dto);
	}

	public List<TblUsuario> getTblUsuario() {
		return (List<TblUsuario>) repository.findAll();
	}

	public TblUsuario getTblUsuarioById(int id) {
		Optional<TblUsuario> value = repository.findById(id);
		TblUsuario optTblUsuario = new TblUsuario();
		if (value.isPresent()) {
			optTblUsuario = value.get();
		}
//        TblUsuario optTblUsuario = repository.findById(id).get();
		return optTblUsuario;
	}

	public TblUsuario getTblUsuarioByUserName(String username) {
		TblUsuario optTblUsuario = repository.findByUserName(username);
		return optTblUsuario;
	}

	public void actualizaPass(String pass, String mail) {
		DaoGenerico a = new DaoGenerico();
		a.actualizaPass(pass, mail);
	}

}