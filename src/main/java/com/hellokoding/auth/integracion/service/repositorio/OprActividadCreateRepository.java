package com.hellokoding.auth.integracion.service.repositorio;

import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import com.hellokoding.auth.modelo.entities.OprActividadCreate;

public interface OprActividadCreateRepository extends JpaRepository<OprActividadCreate, Integer> {

}


    