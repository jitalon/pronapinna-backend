package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblPlanNacionalRepository;

import com.hellokoding.auth.modelo.entities.TblPlanNacional;
import com.hellokoding.auth.objects.TmpLoadFile;
import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

import java.util.List;
import java.util.Optional;

@Component
public class TblPlanNacionalService {

@Autowired 
TblPlanNacionalRepository repository;

    public void add(TblPlanNacional dto) {
        repository.save(dto);
    }

    public List<TblPlanNacional> getTblPlanNacional() {
        return (List<TblPlanNacional>) repository.findAll();
    }  

    public String insertObjetos(String clave) {
        DaoGenerico a = new DaoGenerico();
        a.crearPlanNacional(clave);
        return "simon";
    }

    public String truncateTmp(){
        DaoGenerico a = new DaoGenerico();
        a.truncateTmp();   
        return "simon";
    }

    public String crearOprPlanNacional(String fechaInicio,String fechaFin){
        DaoGenerico a = new DaoGenerico();
        a.crearOprPlanNacional(fechaInicio,fechaFin);   
        return "simon";
    }

    public int planNacionalActivo(){
        return repository.planNacionalActivo();
    }

    public void actualizaEstatusTblPlanNacional(){
        //repository.actualizaEstatusTblPlanNacional();
        DaoGenerico a = new DaoGenerico();
        a.actualizaEstatusTblPlanNacionalAnual();        
    }

    public List<TmpLoadFile> tmpLoadFile(){
        DaoGenerico a = new DaoGenerico();
        return a.TmpLoadFile();
    }

}