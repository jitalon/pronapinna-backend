package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatEstadoRepository;

import com.hellokoding.auth.modelo.entities.CatEstado;

import java.util.List;
import java.util.Optional;

@Component
public class CatEstadoService {

@Autowired 
CatEstadoRepository repository;

    public List<CatEstado> getCatEstado() {
        return (List<CatEstado>) repository.findAll();
    }
  
}