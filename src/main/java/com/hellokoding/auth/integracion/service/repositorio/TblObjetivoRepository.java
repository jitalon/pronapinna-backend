package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TblObjetivo;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface TblObjetivoRepository extends RepositoryGeneric<TblObjetivo, Integer> {

    @Query("SELECT d FROM TblObjetivo d WHERE d.idEstatus = 1")
    List<TblObjetivo> findAllActivo();

}