package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatMunicipioRepository;

import com.hellokoding.auth.modelo.entities.CatMunicipio;

import java.util.List;
import java.util.Optional;

@Component
public class CatMunicipioService {

@Autowired 
CatMunicipioRepository repository;

    public List<CatMunicipio> getCatMunicipio() {
        return (List<CatMunicipio>) repository.findAll();
    }

    public List<CatMunicipio> getCatMunicipioByIdEstado(int idEstado) {
        return (List<CatMunicipio>) repository.findByIdEstado(idEstado);
    }
    
    public List<CatMunicipio> getCatMunicipioByClave(String clave) {
        return (List<CatMunicipio>) repository.findByClave(clave);
    }

    public void add(CatMunicipio dto) {
        System.out.println("+++++++++++++++++++++++++++++++++++++ AQUI");
        repository.save(dto);
    }
  
}