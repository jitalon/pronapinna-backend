package com.hellokoding.auth.integracion.service.impl;

import com.hellokoding.auth.integracion.service.CatalogosService;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;
import com.hellokoding.auth.objects.Respuesta;
import com.hellokoding.auth.objects.RolDependencia;
import com.hellokoding.auth.objects.GraficaDto;
import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

@Service
public class CatalogosServiceImpl implements CatalogosService {
	
	private Logger logger = Logger.getAnonymousLogger();

	public Respuesta consultarCatalogos(){
		Respuesta respuesta = new Respuesta();
		try{
			respuesta.setClave("SERVICIO");
			respuesta.setMsg("OK");
		}catch(Exception ex){
			logger.log(Level.SEVERE, "Error", ex);
		}
		return respuesta;
	}

	public GraficaDto graficas(){
		DaoGenerico a = new DaoGenerico();
		return a.Graficas();
	}

	public List<RolDependencia> dependenciaRol(){
		//LOG.debug("++++++++++++++++++++++ dependenciaRol Impl ");
		DaoGenerico a = new DaoGenerico();
		return a.dependenciaRol();
	}


}
