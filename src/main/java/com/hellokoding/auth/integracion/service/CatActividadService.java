package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatActividadRepository;

import com.hellokoding.auth.modelo.entities.CatActividad;

import java.util.List;
import java.util.Optional;

@Component
public class CatActividadService {

@Autowired 
CatActividadRepository repository;

    public List<CatActividad> getCatActividad() {
        return (List<CatActividad>) repository.findAll();
    }

    public void add(CatActividad dto) {
        repository.save(dto);
    }
  
}