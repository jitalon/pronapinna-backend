package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.TmpTblEstrategia;

public interface TmpTblEstrategiaRepository extends RepositoryGeneric<TmpTblEstrategia, Integer> {

}