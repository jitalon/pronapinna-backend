package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.TblUsuarioCreateRepository;

import com.hellokoding.auth.modelo.entities.TblUsuarioCreate;

import java.util.List;
import java.util.Optional;

@Component
public class TblUsuarioCreateService {

@Autowired 
TblUsuarioCreateRepository repository;

    public void add(TblUsuarioCreate dto) {
        repository.saveAndFlush(dto);
    }  

    public void actualizaPassword(TblUsuarioCreate dto) {
        System.out.println("************************************************************%%%%% " + dto.getPassword());
        System.out.println("************************************************************%%%%% " + dto.getId());
        repository.actualizaPassword(dto.getPassword(),dto.getId());
    }  

}