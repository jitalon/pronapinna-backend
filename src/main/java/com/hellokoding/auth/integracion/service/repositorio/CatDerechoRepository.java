package com.hellokoding.auth.integracion.service.repositorio;

import com.hellokoding.auth.modelo.entities.CatDerecho;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface CatDerechoRepository extends CrudRepository<CatDerecho, Integer> {

    @Query("SELECT d FROM CatDerecho d WHERE d.idEstatus = 1")
    List<CatDerecho> findAllActivo();

}