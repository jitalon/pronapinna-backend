package com.hellokoding.auth.integracion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hellokoding.auth.integracion.service.repositorio.CatPermisoRepository;

import com.hellokoding.auth.modelo.entities.CatPermiso;

import java.util.List;
import java.util.Optional;

@Component
public class CatPermisoService {

@Autowired 
CatPermisoRepository repository;

    public List<CatPermiso> getCatPermiso() {
        return (List<CatPermiso>) repository.findAll();
    }
  
}