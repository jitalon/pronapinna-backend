package com.hellokoding.auth.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

import com.hellokoding.auth.modelo.entities.OprBitacora;
import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;

import com.hellokoding.auth.integracion.service.repositorio.TblPlanNacionalAnualRepository;
import com.hellokoding.auth.integracion.service.repositorio.OprBitacoraRepository;
import com.hellokoding.auth.objects.Filtros;
import com.hellokoding.auth.utils.ExcelGenerador;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;

//import org.json.JSONArray;
import org.json.JSONObject;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api/customers")
public class FileDownloadController {

	private Logger logger = Logger.getAnonymousLogger();
//	private static final Logger LOG = LoggerFactory.getLogger(FileDownloadController.class);

	@Autowired
	OprBitacoraRepository oprBitacoraRepository;

	@Autowired
	TblPlanNacionalAnualRepository tblPlanNacionalAnualRepository;

	@RequestMapping(value = "/download/bitacoraFiltro.xlsx", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<InputStreamResource> excelBitacoraReportFilters(@RequestBody String dataRow,
			HttpServletRequest request) throws IOException {

		JSONObject jsonObj = null;
		ByteArrayInputStream in = null;
		HttpHeaders headers = new HttpHeaders();

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				Filtros filtro = mapper.readValue(jsonObj.toString(), Filtros.class);

				List<OprBitacora> lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.oprBitacoraFilters(filtro,
						true);
				in = ExcelGenerador.bitacoraToExcel(lstOprBitacora);

				headers.add("Content-Disposition", "attachment; filename=bitacora.xlsx");
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	@RequestMapping(value = "/download/lineaAccion.xlsx", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<InputStreamResource> excelPlanNacionalReport(@RequestBody String dataRow,
			HttpServletRequest request) throws IOException {
		JSONObject jsonObj = null;
		ByteArrayInputStream in = null;
		HttpHeaders headers = new HttpHeaders();

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				Filtros filtro = mapper.readValue(jsonObj.toString(), Filtros.class);
				filtro.setIdEstatus(1);

				List<TblPlanNacionalAnual> lstTblPlanNacionalAnual = (List<TblPlanNacionalAnual>) tblPlanNacionalAnualRepository
						.tblPlanNacionalAnualFilters(filtro);
				in = ExcelGenerador.planNacionalAnualToExcel(lstTblPlanNacionalAnual);

				headers.add("Content-Disposition", "attachment; filename=planNacional.xlsx");
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
//			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));

	}

	@GetMapping(value = "/download/bitacora.xlsx")
	public ResponseEntity<InputStreamResource> excelBitacoraReport() throws IOException {

		List<OprBitacora> lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.findAll();
		ByteArrayInputStream in = ExcelGenerador.bitacoraToExcel(lstOprBitacora);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=bitacora.xlsx");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}
}