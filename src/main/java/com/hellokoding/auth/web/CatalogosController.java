package com.hellokoding.auth.web;

import com.hellokoding.auth.integracion.service.CatalogosService;
import com.hellokoding.auth.integracion.service.CatDependenciaService;
import com.hellokoding.auth.integracion.service.CatPerfilService;
import com.hellokoding.auth.integracion.service.CatPermisoService;
import com.hellokoding.auth.integracion.service.CatPeriodoService;
import com.hellokoding.auth.integracion.service.TblPeriodoService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalService;
import com.hellokoding.auth.integracion.service.TblObjetivoService;
import com.hellokoding.auth.integracion.service.TblEstrategiaService;
import com.hellokoding.auth.integracion.service.TblLineaAccionService;
import com.hellokoding.auth.integracion.service.CatMunicipioService;
import com.hellokoding.auth.integracion.service.CatEstadoService;
import com.hellokoding.auth.integracion.service.CatTipoRespuestaService;
import com.hellokoding.auth.integracion.service.CatDerechoService;
import com.hellokoding.auth.integracion.service.TblPlanNacionalAnualService;
import com.hellokoding.auth.integracion.service.OprActividadService;
import com.hellokoding.auth.integracion.service.CatClasificacionEdadService;
import com.hellokoding.auth.integracion.service.CatPoblacionService;
import com.hellokoding.auth.integracion.service.CatCategoriaService;
import com.hellokoding.auth.integracion.service.CatActividadService;
import com.hellokoding.auth.integracion.service.TblMensajeService;
import com.hellokoding.auth.integracion.service.repositorio.OprBitacoraRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hellokoding.auth.objects.Respuesta;
import com.hellokoding.auth.objects.RolDependencia;

//import org.apache.poi.hpsf.Array;
//import org.json.JSONArray;
import org.json.JSONObject;

import com.hellokoding.auth.modelo.entities.CatDependencia;
//import com.hellokoding.auth.modelo.entities.CatPerfil;
import com.hellokoding.auth.modelo.entities.CatDerecho;
//import com.hellokoding.auth.modelo.entities.CatPermiso;
import com.hellokoding.auth.modelo.entities.CatalogosGral;
//import com.hellokoding.auth.modelo.entities.TblPlanNacional;
//import com.hellokoding.auth.modelo.entities.TblUsuario;
import com.hellokoding.auth.modelo.entities.TblObjetivo;
import com.hellokoding.auth.modelo.entities.TblEstrategia;
import com.hellokoding.auth.modelo.entities.TblLineaAccion;
import com.hellokoding.auth.modelo.entities.CatEstado;
import com.hellokoding.auth.modelo.entities.CatMunicipio;
import com.hellokoding.auth.modelo.entities.CatTipoRespuesta;
//import com.hellokoding.auth.modelo.entities.TblPlanNacionalAnual;
//import com.hellokoding.auth.modelo.entities.OprActividad;
//import com.hellokoding.auth.modelo.entities.CatPeriodo;
import com.hellokoding.auth.modelo.entities.TblPeriodo;
//import com.hellokoding.auth.modelo.entities.CatActividad;
//import com.hellokoding.auth.modelo.entities.CatClasificacionEdad;
//import com.hellokoding.auth.modelo.entities.CatPoblacion;
import com.hellokoding.auth.modelo.entities.CatCategoria;
//import com.hellokoding.auth.modelo.entities.OprPoblacionObjetivo;
//import com.hellokoding.auth.modelo.entities.TblMensaje;
import com.hellokoding.auth.modelo.entities.OprBitacora;

import com.hellokoding.auth.objects.Filtros;
import com.hellokoding.auth.objects.Notificacion;
import com.hellokoding.auth.objects.GraficaDto;

//import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.text.SimpleDateFormat;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes("user")
public class CatalogosController {

//	private static final Logger LOG = LoggerFactory.getLogger(CatalogosController.class);
	private Logger logger = Logger.getAnonymousLogger();

	@Autowired
	OprBitacoraRepository oprBitacoraRepository;

	@Autowired
	CatDependenciaService service;

	@Autowired
	CatPerfilService catPerfilService;

	@Autowired
	CatPermisoService catPermisoService;

	@Autowired
	CatPeriodoService catPeriodoService;

	@Autowired
	TblPeriodoService tblPeriodoService;

	@Autowired
	CatPoblacionService catPoblacionService;

	@Autowired
	CatCategoriaService catCategoriaService;

	@Autowired
	CatActividadService catActividadService;

	@Autowired
	CatDerechoService catDerechoService;

	@Autowired
	CatClasificacionEdadService catClasificacionEdadService;

	@Autowired
	TblPlanNacionalService tblPlanNacionalService;

	@Autowired
	TblEstrategiaService tblEstrategiaService;

	@Autowired
	TblObjetivoService tblObjetivoService;

	@Autowired
	TblLineaAccionService tblLineaAccionService;

	@Autowired
	CatMunicipioService catMunicipioService;

	@Autowired
	CatEstadoService catEstadoService;

	@Autowired
	CatTipoRespuestaService catTipoRespuestaService;

	@Autowired
	TblPlanNacionalAnualService tblPlanNacionalAnualService;

	@Autowired
	OprActividadService oprActividadService;

	@Autowired
	TblMensajeService tblMensajeService;

	@Autowired
	CatalogosService graficaService;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@RequestMapping(value = "/accion/consulta/catDependencia.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> consulta(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<CatDependencia> catDependencia = null;
		try {
			catDependencia = service.getCatDependencia();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catDependencia, status);
	}

	@RequestMapping(value = "/accion/consulta/tblPeriodoActivo.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblPeriodoActivo(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblPeriodo> tblPeriodo = null;
		try {
			tblPeriodo = tblPeriodoService.getTblPeriodoActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(tblPeriodo, status);
	}

	@RequestMapping(value = "/accion/consulta/tblPeriodo/{id}.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblPeriodo(@PathVariable int id, HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblPeriodo> tblPeriodo = null;
		try {
			tblPeriodo = tblPeriodoService.getTblPeriodo(id);
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(tblPeriodo, status);
	}

	@RequestMapping(value = "/accion/guardar/tblPeriodo.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveTblPeriodo(@RequestBody String dataRow, HttpServletRequest request) throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;
//		LOG.debug("++++++++++++++++++++++ " + dataRow);
		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				TblPeriodo tblPeriodoNew = mapper.readValue(jsonObj.toString(), TblPeriodo.class);
				tblPeriodoService.add(tblPeriodoNew);
				tblPlanNacionalAnualService.actualizaFechas(tblPeriodoNew.getPeriodoIni(),
						tblPeriodoNew.getPeriodoFin());

				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}

		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/generales.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catalogosGral(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		CatalogosGral catalogosGral = new CatalogosGral();

		catalogosGral.setCatPerfil(catPerfilService.getCatPerfil());
		catalogosGral.setCatDependencia(service.getCatDependencia());
		catalogosGral.setCatPermiso(catPermisoService.getCatPermiso());
		catalogosGral.setCatPeriodo(catPeriodoService.getCatPeriodo());
		catalogosGral.setCatEstado(catEstadoService.getCatEstado());
		catalogosGral.setCatClasificacionEdad(catClasificacionEdadService.getCatClasificacionEdad());
		catalogosGral.setCatPoblacion(catPoblacionService.getCatPoblacion());
		//
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(ClientCatActividadAppConfig.class);
//		ctx.refresh();
//		CatActividadSoapClient getCatActividadSoapClient = ctx.getBean(CatActividadSoapClient.class);
//		CatActividadResponse catActividadResponse = getCatActividadSoapClient.getCatActividad(0, "0");
//		
//		for (CatActividadDetalle catActividadDetalle: catActividadResponse.getCatActividadDetalle()) {
//			logger.info("ID: "+catActividadDetalle.getId());
//			logger.info("CLAVE: "+catActividadDetalle.getClave());
//			logger.info("DESCRIPCION: "+catActividadDetalle.getDescripcion());
//		}
		//
		catalogosGral.setCatActividad(catActividadService.getCatActividad());
		catalogosGral.setCatTipoRespuesta(catTipoRespuestaService.getCatTipoRespuestaActivo());
		catalogosGral.setCatDerecho(catDerechoService.getCatDerechoActivo());
		catalogosGral.setCatCategoria(catCategoriaService.getCatCategoriaActivo());

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String date = simpleDateFormat.format(new Date());

		catalogosGral.setFecha(date);
		return new ResponseEntity<>(catalogosGral, status);
	}

	@RequestMapping(value = "/accion/guardar/catDependencia.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> save(@RequestBody String dataRow, HttpServletRequest request) throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				CatDependencia dependenciaNew = new CatDependencia();
				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					dependenciaNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				if (jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty())
					dependenciaNew.setClave(jsonObj.get("clave").toString());
				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					dependenciaNew.setDescripcion(jsonObj.get("descripcion").toString());
				service.add(dependenciaNew);

				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}

		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/tblObjetivo.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblObjetivo(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblObjetivo> tblObjetivo = null;
		try {
			tblObjetivo = tblObjetivoService.getTblObjetivoActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(tblObjetivo, status);
	}

	@RequestMapping(value = "/calatolos/guarda/seguridad/tblObjetivo.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveTblObjetivo(@RequestBody String dataRow, HttpServletRequest request) throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				TblObjetivo tblObjetivoNew = mapper.readValue(jsonObj.toString(), TblObjetivo.class);
				tblObjetivoService.add(tblObjetivoNew);

				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/tblLineaAccion.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblLineaAccion(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblLineaAccion> tblLineaAccion = null;
		try {
			tblLineaAccion = tblLineaAccionService.getTblLineaAccionActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(tblLineaAccion, status);
	}

	@RequestMapping(value = "/calatolos/guarda/seguridad/tblLineaAccion.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveTblLineaAccion(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				TblLineaAccion tblLineaAccionNew = mapper.readValue(jsonObj.toString(), TblLineaAccion.class);
				tblLineaAccionService.add(tblLineaAccionNew);
				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/calatolos/guarda/catMunicipio.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveCatMunicipio(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				CatMunicipio catMunicipioNew = mapper.readValue(jsonObj.toString(), CatMunicipio.class);
				catMunicipioService.add(catMunicipioNew);

				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/tblEstrategia.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblEstrategia(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<TblEstrategia> tblEstrategia = null;
		try {
			tblEstrategia = tblEstrategiaService.getTblEstrategiaActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(tblEstrategia, status);
	}

	@RequestMapping(value = "/calatolos/guarda/seguridad/tblEstrategia.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveTblEstrategia(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				ObjectMapper mapper = new ObjectMapper();
				TblEstrategia tblEstrategiaNew = mapper.readValue(jsonObj.toString(), TblEstrategia.class);

				tblEstrategiaService.add(tblEstrategiaNew);
				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/catEstado.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catEstado(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<CatEstado> catEstado = null;
		try {
			catEstado = catEstadoService.getCatEstado();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catEstado, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/catMunicipio/{idEstado}.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catMunicipio(HttpServletRequest request, @PathVariable String idEstado) {

		HttpStatus status = HttpStatus.OK;
		List<CatMunicipio> catMunicipio = null;
		try {
			catMunicipio = catMunicipioService.getCatMunicipioByIdEstado(Integer.parseInt(idEstado));
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catMunicipio, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/catTipoRespuesta.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catTipoRespuesta(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<CatTipoRespuesta> catTipoRespuesta = null;
		try {
			catTipoRespuesta = catTipoRespuestaService.getCatTipoRespuestaActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catTipoRespuesta, status);
	}

	@RequestMapping(value = "/accion/guardar/catTipoRespuesta.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveCatTipoRespuesta(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				CatTipoRespuesta tipoRespuestaNew = new CatTipoRespuesta();
				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					tipoRespuestaNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				if (jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty())
					tipoRespuestaNew.setClave(jsonObj.get("clave").toString());
				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					tipoRespuestaNew.setDescripcion(jsonObj.get("descripcion").toString());
				if (jsonObj.has("idEstatus") && !jsonObj.get("idEstatus").toString().isEmpty())
					tipoRespuestaNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				catTipoRespuestaService.add(tipoRespuestaNew);
				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/catDerecho.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catDerecho(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<CatDerecho> catDerecho = null;
		try {
			catDerecho = catDerechoService.getCatDerechoActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catDerecho, status);
	}

	@RequestMapping(value = "/accion/guardar/catDerecho.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveCatDerecho(@RequestBody String dataRow, HttpServletRequest request) throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				CatDerecho derechoNew = new CatDerecho();
				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					derechoNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				if (jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty())
					derechoNew.setClave(jsonObj.get("clave").toString());
				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					derechoNew.setDescripcion(jsonObj.get("descripcion").toString());
				if (jsonObj.has("idEstatus") && !jsonObj.get("idEstatus").toString().isEmpty())
					derechoNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				catDerechoService.add(derechoNew);
				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/catCategoria.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> catCategoria(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<CatCategoria> catCategoria = null;
		try {
			catCategoria = catCategoriaService.getCatCategoriaActivo();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(catCategoria, status);
	}

	@RequestMapping(value = "/accion/guardar/catCategoria.do", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> saveCatCategoria(@RequestBody String dataRow, HttpServletRequest request)
			throws Exception {

		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		JSONObject jsonObj = null;

		try {
			if (null != dataRow) {
				jsonObj = new JSONObject(dataRow);

				CatCategoria categoriaNew = new CatCategoria();
				if (jsonObj.has("id") && !jsonObj.get("id").toString().isEmpty())
					categoriaNew.setId(Integer.parseInt(jsonObj.get("id").toString()));
				if (jsonObj.has("clave") && !jsonObj.get("clave").toString().isEmpty())
					categoriaNew.setClave(jsonObj.get("clave").toString());
				if (jsonObj.has("descripcion") && !jsonObj.get("descripcion").toString().isEmpty())
					categoriaNew.setDescripcion(jsonObj.get("descripcion").toString());
				if (jsonObj.has("idEstatus") && !jsonObj.get("idEstatus").toString().isEmpty())
					categoriaNew.setIdEstatus(Integer.parseInt(jsonObj.get("idEstatus").toString()));

				catCategoriaService.add(categoriaNew);
				respuesta.setClave("200");
				respuesta.setMsg("Registro guardado correctamente");
			}
		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
			status = HttpStatus.NOT_FOUND;
			respuesta.setClave("0");
			respuesta.setMsg("Problemas al guardar el registro, error " + e.getMessage());
		}
		return new ResponseEntity<>(respuesta, status);
	}

	@RequestMapping(value = "/accion/consulta/informacion/tblMensajeBitacora/{rol}.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> tblMensaje(@RequestBody String dataRow, @PathVariable String rol,
			HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		List<OprBitacora> lstOprBitacora = new ArrayList<OprBitacora>();
		List<Notificacion> lstNotificacion = new ArrayList<Notificacion>();
		JSONObject jsonObj = null;
		Filtros filtros = new Filtros();
		filtros.setIdEstatus(1);

		try {

			if (null != dataRow) {
				if (!rol.equals("ADMINISTRADOR")) {
					jsonObj = new JSONObject(dataRow);
//					LOG.debug("----------------------------------- " + jsonObj.toString());
					if (jsonObj.has("idDependencia") && !jsonObj.get("idDependencia").toString().isEmpty())
						filtros.setDependencia(Integer.parseInt(jsonObj.get("idDependencia").toString()));
				}
			}

//			lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.oprBitacoraFilters2(filtros);
			String estatusPerfil = "";
			boolean bndRegistro = true;
			if (rol.equals("ADMINISTRADOR")) {
				estatusPerfil = "2,4,5";
				bndRegistro = false;
			} else {
				estatusPerfil = "1,2,3,5";
				bndRegistro = true;
			}
			lstOprBitacora = (List<OprBitacora>) oprBitacoraRepository.oprBitacoraFilters2(filtros, bndRegistro);

			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			SimpleDateFormat fechaTabla = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			boolean bndRegistro = true;
			for (OprBitacora oprBitacora : lstOprBitacora) {
//				System.out.println("************************************************");
//				System.out.println(oprBitacora.getTblUsuario().getId() + "  ==  1");
//				System.out.println("------------------------------------------------");
//				bndRegistro = (!bndRegistro);
				if (estatusPerfil.contains(oprBitacora.getAccion().trim())
				/*
				 * || oprBitacora.getAccion().equals("3") && oprBitacora.getTblUsuario().getId()
				 * == 1
				 */) {
					Notificacion notificacion = new Notificacion();
					// notificacion.setDependencia(oprBitacora.getTblUsuario().getCatDependencia().getDescripcion());

					String depCoordinada = "";
					for (CatDependencia dependenciaCoordinador : oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getCatDependenciaCoodinador()) {
						depCoordinada = depCoordinada + dependenciaCoordinador.getDescripcion();
					}

					notificacion.setDependenciaCoodinador(depCoordinada);

					String depCoordinadora = "";
//					for (CatDependencia dependenciaCoordinadora : oprBitacora.getOprActividad()
//							.getTblPlanNacionalAnual().getCatDependenciaCoodinadora()) {
//						depCoordinadora = depCoordinadora + dependenciaCoordinadora.getDescripcion();
//					}
					depCoordinadora = depCoordinadora
							+ oprBitacora.getTblUsuario().getCatDependencia().getDescripcion();
//					System.out.println("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]");
//					System.out.println(depCoordinadora);
//					System.out.println("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[");

					notificacion.setDependencia(depCoordinadora);

					String msj = " \"Correspondiente al periodo ( ";
					msj = msj + simpleDateFormat
							.format(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getFechaInicio());
					msj = msj + " al " + simpleDateFormat
							.format(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getFechaFin());
					msj = msj + " ) perteneciente al Programa Nacional : "
							+ oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
									.getTblEstrategia().getTblObjetivo().getTblPlanNacional().getClave()
							+ "\"";

					notificacion.setClaveAccion(
							oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion().getClave());
					notificacion.setAccionPuntual(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getTblLineaAccion().getDescripcion());
					notificacion.setClaveEst(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getClave());
					notificacion.setEstrategia(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
							.getTblLineaAccion().getTblEstrategia().getDescripcion());
					notificacion.setClaveObj(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getTblObjetivo().getClave());
					notificacion.setObjetivo(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getTblLineaAccion()
							.getTblEstrategia().getTblObjetivo().getDescripcion());
					// notificacion.setActividad(oprBitacora.getOprActividad().getCatActividad().getDescripcion());
					notificacion
							.setActividad(oprBitacora.getOprActividad().getCatActividad().getDescripcion() + " " + msj);
					notificacion.setComentario(oprBitacora.getMensaje());
//					LOG.debug("############################## -------------------- " + oprBitacora.getAccion());
					String accion = "";
					switch (oprBitacora.getAccion()) {
					case "1":
						accion = "Captura";
						break;
					case "2":
						accion = "Validaci\u00f3n";
						break;
					case "3":
						accion = "Resultados";
						break;
					case "4":
						accion = "Autorizaci\u00f3n";
						break;
					default:
						accion = "Concluida";
						break;
					}
					notificacion.setEstatus(accion);
					notificacion.setFecha(oprBitacora.getFecha());
					notificacion.setFechaTabla(fechaTabla.format(oprBitacora.getFecha()));
					notificacion.setIdActividad(oprBitacora.getOprActividad().getCatActividad().getId());
					notificacion
							.setIdPlanNacionalAnual(oprBitacora.getOprActividad().getTblPlanNacionalAnual().getId());
					if (null != oprBitacora.getOprActividad().getTblPlanNacionalAnual().getCatTipoRespuesta())
						notificacion.setTipoRespuesta(oprBitacora.getOprActividad().getTblPlanNacionalAnual()
								.getCatTipoRespuesta().getDescripcion());

					lstNotificacion.add(notificacion);
				}
			}

		} catch (Exception e) {
//			e.printStackTrace();
			logger.log(Level.SEVERE, "Error", e);
		}

		return new ResponseEntity<>(lstNotificacion, status);
	}

	@RequestMapping(value = "/accion/consulta/graficas.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> graficas(HttpServletRequest request) {

		HttpStatus status = HttpStatus.OK;
		GraficaDto grafica = null;
		try {
			grafica = graficaService.graficas();
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(grafica, status);
	}

	@RequestMapping(value = "/accion/consulta/dependenciaRol.do", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "application/json", consumes = "application/json")
	public ResponseEntity<?> dependenciaRol(HttpServletRequest request) {

//		LOG.debug("++++++++++++++++++++++ dependenciaRol ");
		HttpStatus status = HttpStatus.OK;
		List<RolDependencia> dependenciaRol = new ArrayList<RolDependencia>();
		try {
			dependenciaRol = graficaService.dependenciaRol();
//			LOG.debug("++++++++++++++++++++++ dependenciaRol " + dependenciaRol.size());
		} catch (Exception e) {
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(dependenciaRol, status);
	}

}