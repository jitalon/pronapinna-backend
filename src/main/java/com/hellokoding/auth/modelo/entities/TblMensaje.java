package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.FetchType;

import java.util.List;
import java.util.ArrayList;
import java.math.*;

@Entity
@Table(name = "tbl_mensaje")
public class TblMensaje implements Serializable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_mensaje")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_mensaje", allocationSize = 1, name = "sec_tbl_mensaje")
    @Id
    private Integer id;
    private Integer idEstatus;
    private String comentario;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @JoinColumn(name="id_actividad",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private OprActividad oprActividad;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}

	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public OprActividad getOprActividad() {
		return oprActividad;
	}
	public void setOprActividad(OprActividad oprActividad) {
		this.oprActividad = oprActividad;
	}

	public TblMensaje() {
		super();
	}
}
