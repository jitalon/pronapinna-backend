package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.FetchType;

import java.util.List;
import java.util.ArrayList;
import java.math.*;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "opr_actividad")
@JsonIgnoreProperties({"tblPlanNacionalAnual"})
public class OprActividad implements Serializable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_opr_actividad")
    @SequenceGenerator(sequenceName = "pronapinna.sec_opr_actividad", allocationSize = 1, name = "sec_opr_actividad")
    @Id
    private Integer id;

    @JoinColumn(name="id_plan_nacional_anual",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblPlanNacionalAnual tblPlanNacionalAnual;

    @JoinColumn(name="id_periodo_realizacion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatPeriodo catPeriodoRealizacion;

    @JoinColumn(name="id_categoria",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatCategoria catCategoria;

    @JoinColumn(name="id_periodo_programacion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatPeriodo catPeriodoProgramacion;

    @JoinColumn(name="id_cat_actividad",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatActividad catActividad;
 
    private String resultado;
    private String descripcion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "importeAsignado")
    private BigDecimal importeAsignado;

    @Column(name = "total_Mujeres")
    private int totalMujeres;

    @Column(name = "total_Hombres")
    private int totalHombres;

    @Column(name = "unidad_responsable")
    private String unidadResponsable;

    private String cobertura;
    private String observacion;
    private String identificacion;
    @Column(name = "id_estatus")
    private int idEstatus;

    @OneToOne(mappedBy = "oprActividad")
    @JsonManagedReference
    private TblArchivo tblArchivo;

    @OneToMany(mappedBy="oprActividad",fetch=FetchType.LAZY ,cascade=CascadeType.ALL )  //,fetch=FetchType.LAZY ,cascade=CascadeType.ALL 
    @JsonManagedReference
    private List<OprPoblacionObjetivo> oprPoblacionObjetivo;

    @ManyToMany(cascade=CascadeType.REFRESH)
    @JoinTable(
		name="opr_municipio_actividad",
		joinColumns={@JoinColumn(name="id_actividad", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_municipio", referencedColumnName="ID")})
    private List<CatMunicipio> catMunicipio;

    @ManyToMany(cascade=CascadeType.REFRESH)
    @JoinTable(
		name="opr_estado_actividad",
		joinColumns={@JoinColumn(name="id_actividad", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_estado", referencedColumnName="ID")})
    private List<CatEstado> catEstado;

    @ManyToMany(cascade=CascadeType.REFRESH)
    @JoinTable(
		name="opr_derecho_actividad",
		joinColumns={@JoinColumn(name="id_actividad", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_derecho", referencedColumnName="ID")})
    private List<CatDerecho> catDerecho;

	public List<CatDerecho> getCatDerecho()
	{
		return catDerecho;
	}

	public void setCatDerecho(List<CatDerecho> catDerecho)
	{
		this.catDerecho = catDerecho;
	}

	public List<CatEstado> getCatEstado()
	{
		return catEstado;
	}

	public void setCatEstado(List<CatEstado> catEstado)
	{
		this.catEstado = catEstado;
	}

	public List<CatMunicipio> getCatMunicipio()
	{
		return catMunicipio;
	}

	public void setCatMunicipio(List<CatMunicipio> catMunicipio)
	{
		this.catMunicipio = catMunicipio;
	}

	public List<OprPoblacionObjetivo> getOprPoblacionObjetivo() {
	    return oprPoblacionObjetivo;
	}
	public void addOprPoblacionObjetivo(OprPoblacionObjetivo f) {
	    oprPoblacionObjetivo.add(f);
	}
	public void setOprPoblacionObjetivo(List<OprPoblacionObjetivo> oprPoblacionObjetivo) {
	    this.oprPoblacionObjetivo = oprPoblacionObjetivo;
        }

	public TblPlanNacionalAnual getTblPlanNacionalAnual() {
		return tblPlanNacionalAnual;
	}
	public void setTblPlanNacionalAnual(TblPlanNacionalAnual tblPlanNacionalAnual) {
		this.tblPlanNacionalAnual = tblPlanNacionalAnual;
	}

	public CatActividad getCatActividad() {
		return catActividad;
	}
	public void setCatActividad(CatActividad catActividad) {
		this.catActividad = catActividad;
	}

	public CatPeriodo getCatPeriodoRealizacion() {
		return catPeriodoRealizacion;
	}
	public void setCatPeriodoRealizacion(CatPeriodo catPeriodoRealizacion) {
		this.catPeriodoRealizacion = catPeriodoRealizacion;
	}

	public CatPeriodo getCatPeriodoProgramacion() {
		return catPeriodoProgramacion;
	}
	public void setCatPeriodoProgramacion(CatPeriodo catPeriodoProgramacion) {
		this.catPeriodoProgramacion = catPeriodoProgramacion;
	}

	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getUnidadResponsable() {
		return unidadResponsable;
	}
	public void setUnidadResponsable(String unidadResponsable) {
		this.unidadResponsable = unidadResponsable;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public int getTotalMujeres() {
		return totalMujeres;
	}
	public void setTotalMujeres(int totalMujeres) {
		this.totalMujeres = totalMujeres;
	}

	public int getTotalHombres() {
		return totalHombres;
	}
	public void setTotalHombres(int totalHombres) {
		this.totalHombres = totalHombres;
	}

	public BigDecimal getImporteAsignado() {
		return importeAsignado;
	}
	public void setImporteAsignado(BigDecimal importeAsignado) {
		this.importeAsignado = importeAsignado;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}

	public CatCategoria getCatCategoria() {
		return catCategoria;
	}
	public void setCatCategoria(CatCategoria catCategoria) {
		this.catCategoria = catCategoria;
	}	

	public void setTblArchivo(TblArchivo tblArchivo) {
		this.tblArchivo = tblArchivo;
	}

	public TblArchivo getTblArchivo() {
		return tblArchivo;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



  public OprActividad() {
		super();
        oprPoblacionObjetivo=new ArrayList();
  }
}
