package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
@Entity
@Table(name = "tmp_tbl_linea_accion")
public class TmpTblLineaAccion {
  @Id
  private Integer id;
  @Column(name = "clave_linea_accion")
  private String claveLineaAccion;
  @Column(name = "clave_estrategia")
  private String claveEstrategia;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getClaveEstrategia() {
    return claveEstrategia;
  }
  public void setClaveEstrategia(String claveEstrategia) {
    this.claveEstrategia = claveEstrategia;
  }
  public String getClaveLineaAccion() {
    return claveLineaAccion;
  }
  public void setClaveLineaAccion(String claveLineaAccion) {
    this.claveLineaAccion = claveLineaAccion;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public TmpTblLineaAccion (Integer id, String claveLineaAccion, String claveEstrategia, String descripcion) {
    super();
    this.id = id;
    this.claveLineaAccion = claveLineaAccion;
    this.claveEstrategia = claveEstrategia;
    this.descripcion = descripcion;
  }
  public TmpTblLineaAccion() {
    super();
  }
  
  
}