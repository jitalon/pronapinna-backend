package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import java.util.List;
import java.math.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "opr_poblacion_objetivo")
//@JsonIgnoreProperties({"oprActividad"})
public class OprPoblacionObjetivo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_opr_poblacion_objetivo")
    @SequenceGenerator(sequenceName = "pronapinna.sec_opr_poblacion_objetivo", allocationSize = 1, name = "sec_opr_poblacion_objetivo")
    private Integer id;

    @JoinColumn(name="id_actividad",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    @JsonBackReference
    private OprActividad oprActividad;

    @JoinColumn(name="id_poblacion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatPoblacion catPoblacion;

    @JoinColumn(name="id_clasificacion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatClasificacionEdad catClasificacionEdad;

    private int total; 
    private int totalH; 
    private int totalM; 

        //@JsonIgnore
	public OprActividad getOprActividad() {
		return oprActividad;
	}
	public void setOprActividad(OprActividad oprActividad) {
		this.oprActividad = oprActividad;
	}


	public CatPoblacion getCatPoblacion() {
		return catPoblacion;
	}
	public void setCatPoblacion(CatPoblacion catPoblacion) {
		this.catPoblacion = catPoblacion;
	}

	public CatClasificacionEdad getCatClasificacionEdad() {
		return catClasificacionEdad;
	}
	public void setCatClasificacionEdad(CatClasificacionEdad catClasificacionEdad) {
		this.catClasificacionEdad = catClasificacionEdad;
	}

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

        public int getTotalH() {
		return totalH;
	}
	public void setTotalH(int totalH) {
		this.totalH = totalH;
	}

	public int getTotalM() {
		return totalM;
	}
	public void setTotalM(int totalM) {
		this.totalM = totalM;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

  public OprPoblacionObjetivo() {
	super();
  }
}
