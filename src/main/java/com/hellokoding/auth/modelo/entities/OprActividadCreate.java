package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.FetchType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.List;
import java.util.ArrayList;
import java.math.*;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "opr_actividad")
@JsonIgnoreProperties({"tblPlanNacionalAnual"})
public class OprActividadCreate implements Serializable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_opr_actividad")
    @SequenceGenerator(sequenceName = "pronapinna.sec_opr_actividad", allocationSize = 1, name = "sec_opr_actividad")
    @Id
    private Integer id;

    @JoinColumn(name="id_plan_nacional_anual",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblPlanNacionalAnual tblPlanNacionalAnual;

    @JoinColumn(name="id_categoria",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatCategoria catCategoria;

    @JoinColumn(name="id_periodo_programacion",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatPeriodo catPeriodoProgramacion;

    @JoinColumn(name="id_cat_actividad",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatActividad catActividad;

    private String identificacion;
    private String descripcion;
    private String cobertura;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "total_Mujeres")
    private int totalMujeres;

    @Column(name = "total_Hombres")
    private int totalHombres;

    @Column(name = "id_estatus")
    private int idEstatus;

    @ManyToMany(cascade=CascadeType.REFRESH)
    @JoinTable(
		name="opr_derecho_actividad",
		joinColumns={@JoinColumn(name="id_actividad", referencedColumnName="ID")},
		inverseJoinColumns={@JoinColumn(name="id_derecho", referencedColumnName="ID")})
    private List<CatDerecho> catDerecho;

	public List<CatDerecho> getCatDerecho()
	{
		return catDerecho;
	}

	public void setCatDerecho(List<CatDerecho> catDerecho)
	{
		this.catDerecho = catDerecho;
	}


	public TblPlanNacionalAnual getTblPlanNacionalAnual() {
		return tblPlanNacionalAnual;
	}
	public void setTblPlanNacionalAnual(TblPlanNacionalAnual tblPlanNacionalAnual) {
		this.tblPlanNacionalAnual = tblPlanNacionalAnual;
	}

	public CatActividad getCatActividad() {
		return catActividad;
	}
	public void setCatActividad(CatActividad catActividad) {
		this.catActividad = catActividad;
	}

	public CatPeriodo getCatPeriodoProgramacion() {
		return catPeriodoProgramacion;
	}
	public void setCatPeriodoProgramacion(CatPeriodo catPeriodoProgramacion) {
		this.catPeriodoProgramacion = catPeriodoProgramacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(int idEstatus) {
		this.idEstatus = idEstatus;
	}	
	public int getTotalHombres() {
		return totalHombres;
	}
	public void setTotalHombres(int totalHombres) {
		this.totalHombres = totalHombres;
	}

	public int getTotalMujeres() {
		return totalMujeres;
	}
	public void setTotalMujeres(int totalMujeres) {
		this.totalMujeres = totalMujeres;
	}

	public CatCategoria getCatCategoria() {
		return catCategoria;
	}
	public void setCatCategoria(CatCategoria catCategoria) {
		this.catCategoria = catCategoria;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}	


  public OprActividadCreate() {
		super();
  }
}
