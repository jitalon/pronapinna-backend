package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import java.util.List;

@Entity
@Table(name = "tbl_linea_accion")
public class TblLineaAccion {

  @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_linea_accion")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_linea_accion", allocationSize = 1, name = "sec_tbl_linea_accion")
    private Integer id;
    private String clave;
    private String descripcion;
    @Column(name = "id_estatus")
    private Integer idEstatus;
    
    @JoinColumn(name="id_estategia",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblEstrategia tblEstrategia;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public TblEstrategia getTblEstrategia() {
        return tblEstrategia;
    }
    public void setTblEstrategia(TblEstrategia tblEstrategia) {
        this.tblEstrategia = tblEstrategia;
    }

    public int getIdEstatus() {
      return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
      this.idEstatus = idEstatus;
    }

    public TblLineaAccion (Integer id, String clave, String descripcion, TblEstrategia tblEstrategia, Integer idEstatus) {
        super();
        this.id = id;
        this.clave = clave;
        this.descripcion = descripcion;
        this.tblEstrategia = tblEstrategia;
        this.idEstatus = idEstatus;
    }
    public TblLineaAccion() {
        super();
    }
  
}