package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import java.util.List;

@Entity
@Table(name = "cat_estado")
public class CatEstado {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_estado")
  @SequenceGenerator(sequenceName = "pronapinna.sec_cat_estado", allocationSize = 1, name = "sec_cat_estado")
  private Integer id;
  private String clave;
  private String descripcion;

/*  @OneToMany(cascade =CascadeType.ALL,mappedBy="catEstado")
  private List<CatMunicipio> catMunicipio;

  public List<CatMunicipio> getCatMunicipio()
  {
	return catMunicipio;
  }
  public void setCatMunicipio(List<CatMunicipio> catMunicipio)
  {
		this.catMunicipio = catMunicipio;
  }*/

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }
  public CatEstado (Integer id, String descripcion, String clave) {
    super();
    this.id = id;
    this.descripcion = descripcion;
    this.clave = clave;
  }
  public CatEstado() {
    super();
  }
  
  
}