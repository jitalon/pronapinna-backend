package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "cat_dependencia")
public class CatDependencia {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_dependencia")
  @SequenceGenerator(sequenceName = "pronapinna.sec_cat_dependencia", allocationSize = 1, name = "sec_cat_dependencia")
  private Integer id;
  private String clave;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }
  public CatDependencia (Integer id, String descripcion, String clave) {
    super();
    this.id = id;
    this.descripcion = descripcion;
    this.clave = clave;
  }
  public CatDependencia() {
    super();
  }
  
  
}