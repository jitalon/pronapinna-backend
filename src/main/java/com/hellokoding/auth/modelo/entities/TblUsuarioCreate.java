package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tbl_usuario")
public class TblUsuarioCreate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_tbl_usuario")
    @SequenceGenerator(sequenceName = "pronapinna.sec_tbl_usuario", allocationSize = 1, name = "sec_tbl_usuario")
    private Integer id;
    private String usuario;
    private String nombre;
    private String password;
    private String email;
    private String telefono;

    @Column(name = "apellido_paterno")
    private String apellidoPaterno;

    @Column(name = "apellido_materno")
    private String apellidoMaterno;

    @Column(name = "id_estatus")
    private Integer idEstatus;
   
    @JoinColumn(name="id_perfil",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatPerfil catPerfil;

    @JoinColumn(name="id_dependencia",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private CatDependencia catDependencia;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
        public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public CatPerfil getCatPerfil() {
		return catPerfil;
	}
	public void setCatPerfil(CatPerfil catPerfil) {
		this.catPerfil = catPerfil;
	}
	public CatDependencia getCatDependencia() {
		return catDependencia;
	}
	public void setCatDependencia(CatDependencia catDependencia) {
		this.catDependencia = catDependencia;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

    public int getIdEstatus() {
      return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
      this.idEstatus = idEstatus;
    }

	public TblUsuarioCreate(){
		super();
        }
}
