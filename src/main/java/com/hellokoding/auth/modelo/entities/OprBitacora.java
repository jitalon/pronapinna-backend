package com.hellokoding.auth.modelo.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.FetchType;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.List;
import java.util.ArrayList;
import java.math.*;

import lombok.Data;

@Entity
@Table(name = "opr_bitacora")
@Data
public class OprBitacora implements Serializable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_opr_bitacora")
    @SequenceGenerator(sequenceName = "pronapinna.sec_opr_bitacora", allocationSize = 1, name = "sec_opr_bitacora")
    @Id
    private Integer id;

    @JoinColumn(name="id_actividad",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private OprActividad oprActividad;

    @JoinColumn(name="id_usuario",unique=true)
    @OneToOne(cascade=CascadeType.REFRESH)
    private TblUsuario tblUsuario;

    private String mensaje;
    private String accion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "id_estatus")
    private Integer idEstatus;

	public int getIdEstatus() {
    	   return idEstatus;
	}

	public void setIdEstatus(int idEstatus) {
	   this.idEstatus = idEstatus;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public OprActividad getOprActividad() {
		return oprActividad;
	}
	public void setOprActividad(OprActividad oprActividad) {
		this.oprActividad = oprActividad;
	}
	
	public TblUsuario getTblUsuario() {
		return tblUsuario;
	}
	public void setTblUsuario(TblUsuario tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public OprBitacora() {
		super();
	}
}
