package com.hellokoding.auth.modelo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import java.util.List;

@Entity
@Table(name = "cat_actividad")
public class CatActividad {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_cat_actividad")
  @SequenceGenerator(sequenceName = "pronapinna.sec_cat_actividad", allocationSize = 1, name = "sec_cat_actividad")
  private Integer id;
  private String clave;
  private String descripcion;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getDescripcion() {
    return descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  public String getClave() {
    return clave;
  }
  public void setClave(String clave) {
    this.clave = clave;
  }

  public CatActividad() {
    super();
  }
  
  
}