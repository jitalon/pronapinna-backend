	package com.hellokoding.auth.controladores;
	
import com.hellokoding.auth.integracion.service.TblUsuarioService;
import com.hellokoding.auth.integracion.service.TblUsuarioCreateService;
import com.hellokoding.auth.objects.PasswordForgotDto;
import com.hellokoding.auth.objects.Respuesta;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;



import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Controller
@RequestMapping("/recuperacion")
public class PassRecovery {


	@Autowired
	TblUsuarioService tblUsuarioService;

	@Autowired
	TblUsuarioCreateService tblUsuarioCreateService;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	private String hasPassword(String password) {

		PasswordEncoder passwordEncoder1 = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder1.encode(password);

		return hashedPassword;
	}

	@ModelAttribute("forgotPasswordForm")
	public PasswordForgotDto forgotPasswordDto() {
		return new PasswordForgotDto();
	}

	@RequestMapping( method = RequestMethod.POST, produces = "application/json", consumes = "application/json")	
	public ResponseEntity<?> processForgotPasswordForm(@RequestBody String dataRow,
			HttpServletRequest request) {

		JSONObject jsonObj = null;
		jsonObj = new JSONObject(dataRow);
		String mailRecovery =jsonObj.get("email").toString();
		//String mailRecovery = request.getParameter("email");
		
		HttpStatus status = HttpStatus.OK;
		Respuesta respuesta = new Respuesta();
		System.out.println("antes de actualizar la tabla ");
		
		if (null != mailRecovery && mailRecovery.trim().length() > 0) {
			String randomStr = RandomStringUtils.random(8, "0123456789abcdefghijklmnopqrstuvwxyz");
			System.out.println("*********************** :::::::::::::::::::::::: **************************" + randomStr);
			tblUsuarioService.actualizaPass(hasPassword(randomStr), mailRecovery);
			System.out.println("---------------------------------- " + mailRecovery);
			respuesta.setClave("200");
			respuesta.setMsg("Se restableció con éxito tu contraseña! ------ " 
					+ randomStr+ " ------ " + mailRecovery);
			System.out.println("todo ok ");
		
		} else {
			respuesta.setClave("500");
			respuesta.setMsg("El correo electr\\u00f3nico es obligatorio");
			System.out.println("No todo ok ");
		}
		System.out.println("listo para retornar ");
		return new ResponseEntity<>(respuesta, status);
	}

}