package com.hellokoding.auth.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EnvioNotificacion {
	private static Logger logger = Logger.getAnonymousLogger();

    public static void Envio(int estatus, String mensaje, String email) {

        final String username = "lgskam23@gmail.com";
        final String password = getEncryptedPass();


        Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("pronapina@segob.gob.mx"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(email)
            );
            message.setSubject("Mensaje del sistema Pronapina");
            message.setText(mensaje);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
			logger.log(Level.SEVERE, "Error", e);
        }
    }

	private static String getEncryptedPass() {
		// TODO Auto-generated method stub
		return "Lgskam7193";
	}

}