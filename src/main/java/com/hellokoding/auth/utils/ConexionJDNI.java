package com.hellokoding.auth.utils;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.*;
import javax.sql.*;

import com.hellokoding.auth.integracion.service.dao.DaoGenerico;

public class ConexionJDNI {
	private Logger logger = Logger.getAnonymousLogger();

	public Connection ConexionJDNIextra() {
		Properties sipinnaSetup = new Properties();
		Connection result = null;
		try {
			logger.log(Level.INFO, "1");
			sipinnaSetup.load(DaoGenerico.class.getResourceAsStream("/application.properties"));
			logger.log(Level.INFO, "2");
			String url = sipinnaSetup.getProperty("spring.datasource.url");
			logger.log(Level.INFO, "3");
			Properties props = new Properties();
			logger.log(Level.INFO, "4");
			props.setProperty("user", sipinnaSetup.getProperty("spring.datasource.username"));
			logger.log(Level.INFO, "5");
			props.setProperty("password",sipinnaSetup.getProperty("spring.datasource.password"));
			logger.log(Level.INFO, "6");
			//props.setProperty("ssl","true");
			result = DriverManager.getConnection(url, props);
			logger.log(Level.INFO, "7");
		} catch (SQLException ex) {
			System.out.println("SQLException");
			logger.log(Level.SEVERE, "Cannot get connection: ", ex);
		} catch (IOException e) {
			System.out.println("IOException");
			logger.log(Level.SEVERE, "Error JDNI: ", e);
		} catch (Exception e) {
			System.out.println("Exception");
			logger.log(Level.SEVERE, "Error JDNI: ", e);
		}
		return result;
	}

	public Connection ConexionJDNI() {
		Properties sipinnaSetup = new Properties();
		Connection result = null;
		try {
			sipinnaSetup.load(DaoGenerico.class.getResourceAsStream("/application.properties"));
			String DATASOURCE_CONTEXT = sipinnaSetup.getProperty("spring.datasource.jndi-name");
			Context initialContext = new InitialContext();
			DataSource datasource = (DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
			if (datasource != null) {
				result = datasource.getConnection();
			} else {
				logger.log(Level.SEVERE, "Failed to lookup datasource.");
			}
		} catch (NamingException ex) {
			logger.log(Level.SEVERE, "Cannot get connection: ", ex);
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "Cannot get connection: ", ex);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error JDNI: ", e);
		}
		return result;
	}
	
	

}
